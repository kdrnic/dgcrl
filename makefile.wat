TARGET_EXEC=gameow.exe
TARGET_PATH=.

# Target executable name
TARGET=$(TARGET_PATH)/$(TARGET_EXEC)

# Directories
WATCOM_DIR=/usr/bin/watcom

# Target system
SYSTEM=dos32a

# C compiler
CC=owcc

# C compiler flags
CCFLAGS=&
	-za99&
	-b $(SYSTEM)&

# C source files
SOURCES=&
	src/draw.c&
	src/ents.c&
	src/input.c&
	src/main.c&
	src/map.c&
	src/msg.c&
	src/player.c&
	src/think.c&

# Header includes
INCLUDES=&
	-I$(WATCOM_DIR)/h&

# Main target
$(TARGET_EXEC): .SYMBOLIC
	$(CC) $(CCFLAGS) $(INCLUDES) $(LIBS) $(SOURCES) -o$(TARGET) -fd=$(TARGET_EXEC).lnk
