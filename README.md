# DOS Game Club code pong #

A cooperative DOS game project.
The idea is to chaotically develop a game for DOS machines, with a "code pong" or "stone soup" strategy where each person takes a turn adding to it.
It is started by kdrnic et al from the DOS Game Club community, and is inspired by an [earlier #ludumdare project](https://openfu.com/codepong/) (the AfterNET IRC channel).

The game itself is roguelike-ish (but no hard rules) and has the following features:
* ASCII graphics, so no assets are necessary
* Procedurally generated, again so no assets are necessary
* Written in plain C99
* Targets the **DJGPP** compiler/environment, alternatively it also supports **OpenWatcom v2**
* Turn-based, for simplicity

## How to participate ##

Know the community links:

* [DOS Game Club Mastodon](https://dosgame.club/)
* [DOS Game Club IRC channel](https://www.dosgameclub.com/chat/) or join AfterNET/#dosgameclub if you already have an IRC client
* [DOS Game Club forums](https://www.dosgameclub.com/) and [thread about this project](https://www.dosgameclub.com/forums/topic/dos-game-club-code-pong/)

Follow those steps:

1. Announce that you're next
	* Take the baton: [Announce it on the forums](https://www.dosgameclub.com/forums/topic/dos-game-club-code-pong/), and optionally hang around the IRC channel.
		* If someone already has the baton, wait for them to commit it or relinquish the baton.
		* If you can't set up an account on the forums, due to email issues, join the IRC channel and pester Tijn and rnlf.
	* Once you have the baton, try to make your changes and upload them within a couple hours - the older Ludum Dare code pong had the baton autoexpire after one hour.
2. Pull/clone from/the repository
	* Alternatively you can just download a ZIP file if you aren't friends with git.
3. Improve the game
	* Add a feature, improve an existing one, add a whole new system, the world is your oyster.
	* There are **no** rules or goals other than improving without breaking the ideas of others in a stone soup fashion.
	* Embrace the ensuing chaos.
4. Commit your changes and make a pull request.
	* [Describe your changes on the changelog](CHANGELOG.md).
	* If you aren't friends with git, beg someone to take a ZIP file and do this for you.
5. Announce that you're done
	* Pass on the baton: [Announce it on the forums](https://www.dosgameclub.com/forums/topic/dos-game-club-code-pong/) so the next person can take it.
	* It is not a hard rule but the idea is to not have two rounds of changes by the same person.

## Build/run instructions ##

### DJGPP ###
Only DJGPP is necessary to build it. A Docker container is provided with a full DJGPP install in case this is useful; see [Docker instructions](./docs/docker.md) for more.

A simple-ish GNU makefile is included, which should be cross-platform although I've tested it only fleetingly.

To run the game, you will need a copy of CWSDPMI.EXE and either a real DOS machine or DOSBox/some other emulator.

### OpenWatcom v2 ###
OpenWatcom v2 as installed has all necessary to build the game. It uses the alternate **makefile.wat**.

### Windows users ###
If you use Windows, I have prepared a ZIP file containing DJGPP, make, CWSDPMI, DOSBox, and OpenWatcom v2, plus 3 shell script files which you can click to build and run the game. You can [download it here](https://drive.google.com/file/d/1IniSRYPzZbZ08XpoAY_rKR8K20mXhfx5).

## Etc. ##

### License ###
The code is **MIT-licensed**. By submitting your changes you agree to have them licensed so. See [LICENSE.txt](LICENSE.txt).

### Wishlist ###
A **wishlist** of planned or suggested features is kept. See and edit [WISHLIST.md](WISHLIST.md).

### Coding standards ###
Just try to write simple, understandable C. If you have any trouble with programming or the language you can ask away on the IRC channel.

Coding style wise, use tabs for indentation, for they are objectively superior. Try to copy the style already in place. All the rest is not of great importance and we can fix it if it gets messed up.
