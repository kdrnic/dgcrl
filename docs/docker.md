# Building with Docker

Building the project with Docker allows you to avoid installing DJGPP on your host machine. This can be useful if you:

* don't want to install djgpp due to compatibility concerns;
* can't find a usable installation of djgpp;
* don't want to worry about building DJGPP.

## Preliminaries

You'll need an up-to-date install of Docker.

## TL;DR

```bash
# Pull container.
REPOSITORY="$(
  git remote get-url origin \
    | head -n 1 \
    | sed -e 's%.*\(https://\|git@\)\([^ ]\+\).*%\2%g' -e 's%:%/%g'
)"
REGISTRY="registry.$REPOSITORY"
DJGPP_IMAGE="$REGISTRY:djgpp"
docker pull "$DJGPP_IMAGE"

# Build the game in the container.
docker run -d -v "$(pwd):/app" -w "/app" --name=codepong "$DJGPP_IMAGE"
docker exec -it codepong make
```

## Fetch the container image

```bash
REPOSITORY="$(
  git remote get-url origin \
    | head -n 1 \
    | sed -e 's%.*\(https://\|git@\)\([^ ]\+\).*%\2%g' -e 's%:%/%g'
)"
REGISTRY="registry.$REPOSITORY"
DJGPP_IMAGE="$REGISTRY:djgpp"
docker pull "$DJGPP_IMAGE"
```

## Create the container

```bash
docker run \
  --detach=true \
  --volume "$(pwd):/app" \
  --workdir "/app" \
  --name="${CONTAINER_NAME:-codepong}" \
  "$DJGPP_IMAGE"
```

## Attach a shell to the container

```bash
docker exec \
  --interactive \
  --tty \
  "${CONTAINER_NAME:-codepong}"
```

## Start/stop the container

```bash
docker start "${CONTAINER_NAME:-codepong}"
docker stop "${CONTAINER_NAME:-codepong}"
```
