/*
input.c

Input-handling routines

SUGGESTIONS/WISHLIST/TODO
-Support other environments than DJGPP
-
*/

#include <conio.h>
#include <ctype.h>

#include "dgcrl.h"

/**
 * List of key bindings
 */
static const keybind_t keybinds[] = {
	/* vi style movement keys */
	{.action = player_go, .key ='y', .dints = {-1,-1}},
	{.action = player_go, .key ='u', .dints = { 1,-1}},
	{.action = player_go, .key ='h', .dints = {-1, 0}},
	{.action = player_go, .key ='j', .dints = { 0, 1}},
	{.action = player_go, .key ='k', .dints = { 0,-1}},
	{.action = player_go, .key ='l', .dints = { 1, 0}},
	{.action = player_go, .key ='b', .dints = {-1, 1}},
	{.action = player_go, .key ='n', .dints = { 1, 1}},
	/* numpad movement */
	{.alias = 'y', .key = '7'},
	{.alias = 'k', .key = '8'},
	{.alias = 'u', .key = '9'},
	{.alias = 'h', .key = '4'},
	{.alias = 'l', .key = '6'},
	{.alias = 'b', .key = '1'},
	{.alias = 'j', .key = '2'},
	{.alias = 'n', .key = '3'},
	/* arrow keys */
	{.alias = 'k', .special = 1, .key = 72},
	{.alias = 'h', .special = 1, .key = 75},
	{.alias = 'l', .special = 1, .key = 77},
	{.alias = 'j', .special = 1, .key = 80},
	/* ascend and descend */
	{.action = player_use_stairs, .key = '<', .dints = {-1},},
	{.action = player_use_stairs, .key = '>', .dints = { 1},},
	/* etc. */
	{.action = player_quit, .key = 27}, /* ESC */
};

/**
 * Recursive function to find binding associated with key
 */
static const keybind_t *find_keybind(int ch, int special)
{
	int i;
	int ch_u = ch, ch_l = ch;
	const keybind_t *kb;
	
	if(!special && isalpha(ch)){
		ch_l = toupper(ch);
		ch_u = tolower(ch);
	}
	
	for(i = 0; i < ARR_SIZE(keybinds); i++){
		kb = keybinds + i;
		if(kb->special != special) continue;
		if(kb->key == ch) break;
		if(!kb->match_case && (kb->key == ch_l || kb->key == ch_u)) break;
	}
	
	if(i < ARR_SIZE(keybinds)){
		if(kb->alias){
			return find_keybind(kb->alias, 0);
		}
		return kb;
	}
	return 0;
}

/**
 * Handle input for one turn
 */
void do_input(void)
{
	int ch = getch();
	int special = 0;
	const keybind_t *kb;
	
	if(ch == 0 || ch == 0xE0){
		special = 1;
		ch = getch();
	}
	
	kb = find_keybind(ch, special);
	if(kb){
		kb->action(kb, ch);
	}
}
