/*
msg.c

Message/text log handling

SUGGESTIONS/WISHLIST/TODO
-
*/

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

#include "dgcrl.h"

/**
 * Lines of text messages
 */
char msg_lines[MSGS_MAX][MSG_LEN];

/**
 * Current line, goes up to MSGS_MAX-1 and goes around
 */
int msg_curr;

/**
 * Formatted-print to a message line
 */
void msg_printf(const char *txt, ...)
{
	va_list args;
	va_start(args, txt);
	vsnprintf(msg_lines[msg_curr], MSG_LEN, txt, args);
	va_end(args);
	
	msg_curr++;
	if(msg_curr == MSGS_MAX) msg_curr = 0;
}
