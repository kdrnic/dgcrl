/*
player.c

Player character related stuff

SUGGESTIONS/WISHLIST/TODO
-Classic roguelike actions:
--pick up/drop
--equip/take off/wield/wear/remove
--use/eat/quaff/fire
--look
--open/close doors
--rest/wait
-
*/

#include "dgcrl.h"

/**
 * Pointer to player data
 * Hardcoded to always be the first entity on the linked list
 */
ent_t *player = &ents[PLAYER_IDX];

/**
 * Set to quit game
 */
int game_over = 0;

/**
 * ACTION CALLBACK for player movement
 */
void player_go(const keybind_t *k, int ch)
{
	int newx = player->x + k->dints[0];
	int newy = player->y + k->dints[1];
	
	ent_try_move(player, newx, newy);
}

/**
 * ACTION CALLBACK for player going up/down stairs
 */
void player_use_stairs(const keybind_t *k, int ch)
{
	int dir = k->dints[0];
	if(map[player->x][player->y] != ((dir < 0) ? '<' : '>')){
		msg_printf("You can't %s here!", (dir < 0) ? "ascend" : "descend");
		return;
	}
	if(map_floor == 1 && dir < 0){
		msg_printf("You aren't ready to leave the dungeon!");
		return;
	}
	
	map_floor += dir;
	map_gen();
	msg_printf("You follow the stairs to another floor...");
}

/**
 * ACTION CALLBACK for quitting the game
 */
void player_quit(const keybind_t *k, int ch)
{
	game_over = 1;
}
