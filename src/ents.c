/*
ents.c

Entity-related stuff, such as generic/utility routines

SUGGESTIONS/WISHLIST/TODO
-
*/

#include <stdlib.h>

#include "dgcrl.h"

/**
 * Array of ent structs
 * Example loop to iterate over the linked list:
 * 
 * 	for(int i = PLAYER_IDX; i >= 0; i = ents[i].idx_next){
 * 		ent_t *curr_ent = ents + i;
 * 		[...]
 * 	}
 */
ent_t ents[ENTS_MAX];

/**
 * Returns a new entity already placed in the linked list
 */
ent_t *ent_new(void)
{
	/* Mark all as free */
	for(int i = 0; i < ENTS_MAX; i++){
		ents[i].free = 1;
	}
	
	/* Mark all in the linked list as not free */
	for(int i = PLAYER_IDX; i >= 0; i = ents[i].idx_next){
		ents[i].free = 0;
	}
	
	/* Find first free entity */
	for(int i = 0; i < ENTS_MAX; i++){
		if(ents[i].free){
			/* Add to linked list, just after the player */
			ents[i].idx_prev = PLAYER_IDX;
			ents[i].idx_next = player->idx_next;
			player->idx_next = i;
			
			/* Return pointer */
			return ents + i;
		}
	}
	
	/*
	Not found
	TODO: should be a fatal error
	*/
	return 0;
}

/**
 * Removes an entity from the linked list, freeing it
 */
void ent_free(ent_t *e)
{
	if(e->idx_prev >= 0){
		ents[e->idx_prev].idx_next = e->idx_next;
	}
	if(e->idx_next >= 0){
		ents[e->idx_next].idx_prev = e->idx_prev;
	}
}

/**
 * Returns entity at position, or 0 if none
 */
ent_t *ent_at(int x, int y)
{
	for(int i = PLAYER_IDX; i >= 0; i = ents[i].idx_next){
		if(ents[i].x == x && ents[i].y == y) return ents + i;
	}
	return 0;
}

/**
 * Tries to move an entity, returns true on success
 */
int ent_try_move(ent_t *e, int newx, int newy)
{
	/* Check for walls */
	if(newx < 0 || newx >= MAP_W) return 0;
	if(newy < 0 || newy >= MAP_H) return 0;
	if(map[newx][newy] == '#') return 0;
	
	/* Check for entities */
	const ent_t *at = ent_at(newx, newy);
	if(at && at != e) return 0;
	
	/* All ok, move */
	e->x = newx;
	e->y = newy;
	
	return 1;
}

/**
 * True if ent is the player's
 */
int ent_is_player(const ent_t *e)
{
	return (e - ents) == PLAYER_IDX;
}

/**
 * Set an entity's parameters to be the same as another, but keep position and linked list data
 */
void ent_set(ent_t *e, const ent_t *proto)
{
	ent_t tmp = *e;
	*e = *proto;
	e->idx_next = tmp.idx_next;
	e->idx_prev = tmp.idx_prev;
	e->x = tmp.x;
	e->y = tmp.y;
}

/**
 * Update all entities
 */
void ent_update(void)
{
	for(int i = PLAYER_IDX; i >= 0; i = ents[i].idx_next){
		ent_t *e = ents + i;
		if(e->think_id) think_funcs[e->think_id](e);
		
		/*
		Check for traps
		*/
		if(map[e->x][e->y] == '^'){
			/*
			TODO: replace with damage, once HP are implemented
			*/
			if(ent_is_player(e)){
				msg_printf(MSG_FG_RED "You step on the trap! It hurts!");
			}
			else{
				msg_printf("The %s is killed by the trap.", e->name);
				ent_free(e);
			}
			
			/* Remove the trap */
			map[e->x][e->y] = 0;
		}
	}
}
