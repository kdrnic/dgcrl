/*
think.c

THINK FUNCTIONs for NPCs, monsters, and other such entities

SUGGESTIONS/WISHLIST/TODO
-
*/

#include <stdlib.h>

#include "dgcrl.h"

/**
 * THINK FUNCTION for rats
 */
static void think_rat(ent_t *self)
{
	/*
	Move randomly
	TODO: replace rand with better RNG
	*/
	int dx = (rand() % 3) - 1;
	int dy = (rand() % 3) - 1;
	if(dx || dy){
		ent_try_move(self, self->x + dx, self->y + dy);
	}
	
	/*
	Squeak if near player
	*/
	if(abs(self->x - player->x) <= 1 && abs(self->y - player->y) <= 1){
		msg_printf("The rat squeaks.");
	}
}

/**
 * Array of THINK FUNCTIONs
 * Each index is a think_t
 * index 0 is unused
 */
think_func_t think_funcs[] = {
	[THINK_RAT] = think_rat,
};
