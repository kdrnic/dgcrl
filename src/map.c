/*
map.c

Map handling routines

SUGGESTIONS/WISHLIST/TODO
-Actual random level generator
-
*/

#include <stdlib.h>

#include "dgcrl.h"

/**
 * Tile data
 * TODO: replace int with fancier data structure
 */
int map[MAP_W][MAP_H];

/**
 * Which floor of the dungeon are we in
 */
int map_floor;

/**
 * Generate one level of the dungeon
 */
void map_gen(void)
{
	int x, y, i;
	
	/*
	Free all non-player entities
	*/
	player->idx_next = -1;
	player->idx_prev = -1;
	
	/*
	Generate a large empty square room
	*/
	for(x = 0; x < MAP_W; x++){
		for(y = 0; y < MAP_W; y++){
			if(
				x == 0 || 
				y == 0 ||
				x == MAP_W - 1 ||
				y == MAP_H - 1
			){
				map[x][y] = '#';
			}
			else map[x][y] = 0;
		}
	}
	
	/*
	Place stairs going up
	*/
	map[player->x][player->y] = '<';
	
	/*
	Place stairs going down
	TODO: replace rand() with better RNG
	*/
	x = 0; y = 0;
	while(map[x][y]){
		x = 1 + rand() % (MAP_W - 2);
		y = 1 + rand() % (MAP_H - 2);
	}
	map[x][y] = '>';
	
	/*
	Spawn a few rats
	*/
	int num_rats = rand() % 3;
	const ent_t rat_proto = {
		.symbol = 'r',
		.think_id = THINK_RAT,
		.name = "rat",
	};
	for(i = 0; i < num_rats; i++){
		ent_t *rat = ent_new();
		ent_set(rat, &rat_proto);
		while(!ent_try_move(rat, 1 + rand() % (MAP_W - 2), 1 + rand() % (MAP_H - 2))){}
	}
	
	/*
	Place traps
	TODO: replace rand() with better RNG
	*/
	int num_traps = rand() % 3;
	for(i = 0; i < num_traps; i++){
		x = 0; y = 0;
		while(map[x][y]){
			x = 1 + rand() % (MAP_W - 2);
			y = 1 + rand() % (MAP_H - 2);
		}
		map[x][y] = '^';
	}
}
