/*
dgcrl.h

Declare all global functions and variables here
	(do not add comments to the declaration only to the definition)
Define all shared data types here

SUGGESTIONS/WISHLIST/TODO
-Inventory/items
-
*/

/**
 * Colour values
 * Those match those used by both DJGPP and OpenWatcom v2
 */
typedef enum colour
{
	COL_BLACK,          
	COL_BLUE,
	COL_GREEN,
	COL_CYAN,
	COL_RED,
	COL_MAGENTA,
	COL_BROWN,
	COL_LIGHTGRAY,
	COL_DARKGRAY,
	COL_LIGHTBLUE,
	COL_LIGHTGREEN,
	COL_LIGHTCYAN,
	COL_LIGHTRED,
	COL_LIGHTMAGENTA,
	COL_YELLOW,
	COL_WHITE,
} colour_t;

/**
 * Map dimensions
*/
#define MAP_W 15
#define MAP_H 15

extern int map[MAP_W][MAP_H];
extern int map_floor;

void map_gen(void);

typedef enum think
{
	THINK_NONE = 0,
	THINK_RAT,
} think_t;

/**
 * Data structure for entities such as the player, monsters, NPCs etc.
 */
typedef struct ent
{
	int x, y;               /* position                                    */
	int idx_next, idx_prev; /* linked list                                 */
	int free;               /* used by ent_new - do not read/set elsewhere */
	int symbol;             /* char, such as '@' for player                */
	think_t think_id;       /* index into think_funcs                      */
	const char *name;		/* such as "rat"                               */
} ent_t;

/**
 * Entity 'think' callback
 */
typedef void (*think_func_t)(ent_t *self);

extern think_func_t think_funcs[];

/**
 * Maximum simultaneous entities
 */
#define ENTS_MAX 1000

/**
 * Index of the player entity
 */
#define PLAYER_IDX 0

extern ent_t ents[ENTS_MAX];
extern ent_t *player;

ent_t *ent_new(void);
void ent_free(ent_t *e);
ent_t *ent_at(int x, int y);
int ent_try_move(ent_t *e, int x, int y);
int ent_is_player(const ent_t *e);
void ent_set(ent_t *e, const ent_t *proto);
void ent_update(void);

void draw_init(void);
void draw_finish(void);
void draw(void);

/**
 * Utility macro - size of a static array
 */
#define ARR_SIZE(arr) (sizeof(arr)/sizeof(arr[0]))

/**
 * Data structure for key bindings
 */
typedef struct keybind
{
	void (*action)(const struct keybind *k, int ch); /* action callback                 */
	int key;                                         /* associated key                  */
	int alias;                                       /* is an alias for another bind    */
	int special;                                     /* for a few keys, like arrow keys */
	int match_case;                                  /* requires matching case          */
	int dints[2];                                    /* data integers                   */
} keybind_t;

extern int game_over;

void player_go(const keybind_t *k, int ch);
void player_use_stairs(const keybind_t *k, int ch);
void player_quit(const keybind_t *k, int ch);

void do_input(void);

void game_init(void);
void game_exit(void);

/**
 * Limits for logged messages
 */
#define MSG_LEN 256
#define MSGS_MAX 100

/**
 * Colour escape codes for messages
 * Those match COL_* from colour_t but 1-indexed
 * Example usage:
 * 	msg_printf("This is normal text, " MSG_FG_RED "but this is red");
 */
#define MSG_ESCAPE_FG_COL "\x11"
#define MSG_FG_BLACK        MSG_ESCAPE_FG_COL "\001"
#define MSG_FG_BLUE         MSG_ESCAPE_FG_COL "\002"
#define MSG_FG_GREEN        MSG_ESCAPE_FG_COL "\003"
#define MSG_FG_CYAN         MSG_ESCAPE_FG_COL "\004"
#define MSG_FG_RED          MSG_ESCAPE_FG_COL "\005"
#define MSG_FG_MAGENTA      MSG_ESCAPE_FG_COL "\006"
#define MSG_FG_BROWN        MSG_ESCAPE_FG_COL "\007"
#define MSG_FG_LIGHTGRAY    MSG_ESCAPE_FG_COL "\010"
#define MSG_FG_DARKGRAY     MSG_ESCAPE_FG_COL "\011"
#define MSG_FG_LIGHTBLUE    MSG_ESCAPE_FG_COL "\012"
#define MSG_FG_LIGHTGREEN   MSG_ESCAPE_FG_COL "\013"
#define MSG_FG_LIGHTCYAN    MSG_ESCAPE_FG_COL "\014"
#define MSG_FG_LIGHTRED     MSG_ESCAPE_FG_COL "\015"
#define MSG_FG_LIGHTMAGENTA MSG_ESCAPE_FG_COL "\016"
#define MSG_FG_YELLOW       MSG_ESCAPE_FG_COL "\017"
#define MSG_FG_WHITE        MSG_ESCAPE_FG_COL "\018"

extern char msg_lines[MSGS_MAX][MSG_LEN];
extern int msg_curr;

void msg_printf(const char *txt, ...);
