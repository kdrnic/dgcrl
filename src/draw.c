/*
draw.c

Rendering routines

SUGGESTIONS/WISHLIST/TODO
-Full graphics mode (VGA mode X? mode 12h?)
-Use fg and bg colours
-
*/

#include <conio.h>

#ifdef __WATCOMC__

	#include <graph.h>
	
	/**
	 * Set text mode to 80-column with colour
	 */
	#define VID_SET_C80() _setvideomode(_TEXTC80)
	
	/**
	 * Restore default text mode used by DOS
	 */
	#define VID_SET_DEFAULT() _setvideomode(_DEFAULTMODE)
	
	/**
	 * Clear entire screen
	 */
	#define VID_CLEAR_SCREEN() _clearscreen(_GCLEARSCREEN)
	
	/**
	 * Moves the cursor to column X, row Y. Top left corner is at 1,1 not 0,0.
	 */
	#define VID_SET_TEXT_POS(x, y) _settextposition(y, x)
	
	/**
	 * Sets foreground colour
	 * TODO: not working in OpenWatcom, only DJGPP...
	 */
	#define VID_SET_FG_COL(x) _settextcolor(x)
	
	static struct videoconfig video_config;
#elif defined __DJGPP__

	#include <pc.h>

	#define VID_SET_C80() textmode(C80)
	#define VID_SET_DEFAULT() normvideo()
	#define VID_CLEAR_SCREEN() clrscr()
	#define VID_SET_TEXT_POS(x, y) gotoxy(x, y)
	#define VID_SET_FG_COL(x) textcolor(x)

#endif

#include "dgcrl.h"

/**
 * Screen dimensions
 */
static int scr_cols, scr_rows;

/**
 * Sets up text/video mode and other graphics initialization
 */
void draw_init(void)
{
	/*
	Set text mode to 80 columns, colour
	http://www.delorie.com/djgpp/doc/libc/libc_820.html
	*/

	#ifdef __WATCOMC__

		_getvideoconfig(&video_config);

	#endif

	VID_SET_C80();

	#ifdef __WATCOMC__

		_getvideoconfig(&video_config);
		_settextcursor(0x2000);

		scr_cols = video_config.numtextcols;
		scr_rows = video_config.numtextrows;

	#elif defined __DJGPP__

		scr_cols = ScreenCols();
		scr_rows = ScreenRows();

	#endif
}

/**
 * Restore text/video mode after quitting the game
 */
void draw_finish(void)
{
	VID_SET_DEFAULT();
	VID_CLEAR_SCREEN();
}

/**
 * Screen positions of status info and messages
 */
#define COL_STATS 60
#define ROW_MSGS 20

/**
 * Function to draw everything once a turn
 */
void draw(void)
{
	/*
	Clear entire screen
	*/
	VID_CLEAR_SCREEN();
	
	/*
	Draw the map
	*/
	for(int x = 0; x < MAP_W; x++){
		for(int y = 0; y < MAP_H; y++){
			if(map[x][y]){
				VID_SET_TEXT_POS(x + 1, y + 1);
				putch(map[x][y]);
			}
		}
	}
	
	/*
	Draw ents
	*/
	for(int i = PLAYER_IDX; i >= 0; i = ents[i].idx_next){
		VID_SET_TEXT_POS(ents[i].x + 1, ents[i].y + 1);
		putch(ents[i].symbol);
	}
	
	/*
	Draw stats
	*/
	VID_SET_TEXT_POS(COL_STATS + 1, 1);
	cprintf("Dungeon floor #%d", map_floor);
	
	/*
	Draw messages
	*/
	for(int row = scr_rows, msg_idx = msg_curr; row >= ROW_MSGS; row--, msg_idx--){
		if(msg_idx < 0){
			msg_idx = MSGS_MAX - 1;
		}
		VID_SET_TEXT_POS(1, row);
		
		const char *line = msg_lines[msg_idx];
		for(int col = 0, m_col = 0; col < scr_cols; col++, m_col++){
			if(!line[m_col]) break;
			
			/*
			Handle FG colour escape sequence
			*/
			while(line[m_col] == MSG_ESCAPE_FG_COL[0]){
				m_col++;
				VID_SET_FG_COL(line[m_col++] - 1);
			}
			
			putch(line[m_col]);
		}
		
		/* Restore regular fg colour */
		VID_SET_FG_COL(COL_LIGHTGRAY);
	}
}
