/*
main.c

No explanations needed

SUGGESTIONS/WISHLIST/TODO
-
*/
#include <time.h>
#include <stdlib.h>

#include "dgcrl.h"

/**
 * Setup
 */
void game_init(void)
{
	draw_init();
	
	/*
	Init player
	*/
	player->x = 1;
	player->y = 1;
	player->idx_next = -1;
	player->idx_prev = -1;
	player->symbol = '@';
	
	/*
	Clear messages
	*/
	for(int i = 0; i < MSGS_MAX; i++){
		msg_lines[i][0] = 0;
	}
	msg_curr = 0;
	
	/*
	Generate first level of dungeon
	*/
	map_floor = 1;
	map_gen();
	
	/*
	Seed RNG
	*/
	srand(time(0));
}

/**
 * Close
 */
void game_exit()
{
	draw_finish();
}

/**
 * Main function
 */
int main(int argc, char **argv)
{
	game_init();
	while(!game_over){
		draw();
		do_input();
		ent_update();
	}
	game_exit();
	return 0;
}
