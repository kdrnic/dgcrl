# Changelog #

Add your changes to the **top** of this document (so that reading down one goes from latest to oldest).

Example markdown:

```
## YYYY-MM-DD nickname ##

Added foo, improved bar, implemented a foobar enemy.
```

## 2022-11-02 kdrnic ##

* Added coloured messages
* Added traps, they kill rats and 'hurt' the player (a message is displayed but otherwise no effect)

## 2022-11-01 JaycieErysdren ##

Tried to add some compatibility with Open Watcom v2. It compiles and runs successfully, however the graphics module is a bit buggy so the text doesn't display in the correct places yet.

## 2022-10-29 kdrnic ##

Initial commit, a barebones square dungeon with the player and rats running around.
