# SUGGESTIONS/WISHLIST/TODO #

You can add features and ideas here, either those you may want to implement yourself or those for others to try to add.
Additionally, each source file has its own list on the header comment.

## Classic roguelike actions ##

* pick up/drop
* equip/take off/wield/wear/remove
* use/eat/quaff/fire
* look
* open/close doors
* rest/wait

## Support other environments than DJGPP/OpenWatcom-v2 ##

* There is also one or more versions of GCC with 16-bit x86 support.

## Better/more random level generators 

## VGA graphics ##

One advantage is having square tiles, whereas with text mode the tiles look taller than wide.
