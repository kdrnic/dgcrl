OBJDIR=obj
BINNAME=game
CFLAGS2=$(CFLAGS)

#Change obj and exe name for debug builds
ifeq ($(filter debug,$(MAKECMDGOALS)),debug)
	OBJDIR:=$(OBJDIR)dbg
	CFLAGS2+=-g -O0 -DDEBUG
	BINNAME:=$(BINNAME)d
endif

#Useful flags
CFLAGS2+=-Wall -Wuninitialized -Werror=implicit-function-declaration -Wno-unused -Wstrict-prototypes

C_FILES=$(wildcard src/*.c)
C_OBJECTS=$(patsubst src/%,$(OBJDIR)/%,$(patsubst %.c,%.o,$(C_FILES)))
OBJECTS=$(C_OBJECTS)
HAVE_LIBS=
INCLUDE_PATHS=
LINK_PATHS=
LINK_FLAGS:=$(LINK_FLAGS)
HEADER_FILES=$(wildcard src/*.h)

$(OBJDIR)/%.o: src/%.c $(HEADER_FILES)
	$(CC) $(INCLUDE_PATHS) $(CFLAGS2) $< -c -o $@

$(BINNAME).exe: $(OBJDIR) $(OBJECTS)
	$(CC) $(LINK_PATHS) $(LINK_FLAGS) $(CFLAGS2) $(OBJECTS) -o $(BINNAME).exe $(HAVE_LIBS)

regular: $(BINNAME).exe
debug: $(BINNAME).exe

clean:
	rm -f $(OBJDIR)/*.o

$(OBJDIR):
	mkdir $(OBJDIR)
